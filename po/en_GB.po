# British English translationn of at-spi2-core
# Copyright (C) 2011 at-spi2-core'S COPYRIGHT HOLDER
# This file is distributed under the same license as the at-spi2-core package.
# Bruce Cowan <bruce@bcowan.me.uk>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: at-spi2-core\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-03-08 20:01+0000\n"
"PO-Revision-Date: 2011-03-08 20:02+0100\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: British English <en@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.6.1\n"

#: ../atspi/atspi-misc.c:290
#, c-format
msgid "AT-SPI: Unknown signature %s for RemoveAccessible"
msgstr "AT-SPI: Unknown signature %s for RemoveAccessible"

#: ../atspi/atspi-misc.c:327
#, c-format
msgid "AT-SPI: Error calling getRoot for %s: %s"
msgstr "AT-SPI: Error calling getRoot for %s: %s"

#: ../atspi/atspi-misc.c:485
#, c-format
msgid "AT-SPI: Error in GetItems, sender=%s, error=%s"
msgstr "AT-SPI: Error in GetItems, sender=%s, error=%s"

#: ../atspi/atspi-misc.c:587
#, c-format
msgid ""
"AT-SPI: Called _atspi_dbus_return_accessible_from_message with strange "
"signature %s"
msgstr ""
"AT-SPI: Called _atspi_dbus_return_accessible_from_message with strange "
"signature %s"

#: ../atspi/atspi-misc.c:616
#, c-format
msgid ""
"AT-SPI: Called _atspi_dbus_return_hyperlink_from_message with strange "
"signature %s"
msgstr ""
"AT-SPI: Called _atspi_dbus_return_hyperlink_from_message with strange "
"signature %s"

#: ../atspi/atspi-misc.c:641
#, c-format
msgid "AT-SPI: AddAccessible with unknown signature %s\n"
msgstr "AT-SPI: AddAccessible with unknown signature %s\n"

#: ../atspi/atspi-misc.c:826
msgid "AT-SPI: Could not get the display\n"
msgstr "AT-SPI: Could not get the display\n"

#: ../atspi/atspi-misc.c:844
msgid "AT-SPI: Accessibility bus not found - Using session bus.\n"
msgstr "AT-SPI: Accessibility bus not found - Using session bus.\n"

#: ../atspi/atspi-misc.c:848 ../atspi/atspi-misc.c:857
#, c-format
msgid "AT-SPI: Couldn't connect to bus: %s\n"
msgstr "AT-SPI: Couldn't connect to bus: %s\n"

#: ../atspi/atspi-misc.c:864
#, c-format
msgid "AT-SPI: Couldn't register with bus: %s\n"
msgstr "AT-SPI: Couldn't register with bus: %s\n"

#: ../atspi/atspi-misc.c:1002 ../atspi/atspi-misc.c:1053
#: ../atspi/atspi-misc.c:1094
msgid "The application no longer exists"
msgstr "The application no longer exists"

#: ../atspi/atspi-misc.c:1130
#, c-format
msgid "AT-SPI: expected a variant when fetching %s from interface %s; got %s\n"
msgstr "AT-SPI: expected a variant when fetching %s from interface %s; got %s\n"

#: ../atspi/atspi-misc.c:1136
#, c-format
msgid "atspi_dbus_get_property: Wrong type: expected %s, got %c\n"
msgstr "atspi_dbus_get_property: Wrong type: expected %s, got %c\n"

#: ../atspi/atspi-misc.c:1279
#, c-format
msgid "AT-SPI: Unknown interface %s"
msgstr "AT-SPI: Unknown interface %s"

#: ../atspi/atspi-misc.c:1299
#, c-format
msgid "AT-SPI: expected 2 values in states array; got %d\n"
msgstr "AT-SPI: expected 2 values in states array; got %d\n"

#: ../atspi/atspi-accessible.c:997
msgid "Streamable content not implemented"
msgstr "Streamable content not implemented"

#: ../atspi/atspi-event-listener.c:510
msgid ""
"called atspi_event_listener_register_from_callback with a NULL event_type"
msgstr ""
"called atspi_event_listener_register_from_callback with a NULL event_type"

#: ../atspi/atspi-event-listener.c:777
#, c-format
msgid "Got invalid signature %s for signal %s from interface %s\n"
msgstr "Got invalid signature %s for signal %s from interface %s\n"
